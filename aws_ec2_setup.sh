#!/bin/bash -v

# https://stackoverflow.com/questions/821396/aborting-a-shell-script-if-any-command-returns-a-non-zero-value
set -e

##########
echo 'install anaconda prerequisites...'
# https://docs.anaconda.com/anaconda/install/linux/
sudo apt-get update
sudo apt-get install -y libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6
sudo apt-get install -y gcc # needed for virtualenv

##########  
echo 'install anaconda...'
wget https://repo.anaconda.com/archive/Anaconda3-2020.07-Linux-x86_64.sh
bash Anaconda3-2020.07-Linux-x86_64.sh -b
eval "$($HOME/anaconda3/bin/conda shell.bash hook)"
# echo 'export PATH="/home/ubuntu/anaconda3/bin:$PATH"' >> ~/.bashrc
# source ~/.bashrc && which python
conda init
conda update -y -n base -c defaults conda
conda install -y -c anaconda jupyter
# https://packaging.python.org/tutorials/packaging-projects/
pip install --upgrade setuptools wheel virtualenv

##########
echo 'install selenium + firefox...'
pip install selenium
conda install -y -c conda-forge geckodriver firefox

##########
echo 'install aws tools...' 
pip install awscli boto3
sudo apt-get install -y jq # needed for aws s3api list-objects

##########
echo 'install git...'
sudo apt install -y git
git --version
git config --global user.name "druprivate"
git config --global user.email "damien.russier@gmail.com"
git config --list

##########
echo 'install vim with py3 support...'
pip install vim
vim --version | grep python
# https://github.com/amix/vimrc
git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh

##########
echo 'install docker...'  
# https://docs.docker.com/engine/install/ubuntu/
sudo apt-get update
sudo apt-get install -y \
apt-transport-https \
ca-certificates \
curl \
gnupg-agent \
software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) \
stable"

sudo apt-get update && sudo apt-get install -y docker-ce docker-ce-cli containerd.io
sudo docker run hello-world

##########  
echo 'install other packages and clean...'
sudo apt-get install -y tmux htop
sudo apt autoremove -y

##########
echo 'generate ssh key...'
# https://stackoverflow.com/questions/43235179/how-to-execute-ssh-keygen-without-prompt
ssh-keygen -q -t rsa -N '' -f ~/.ssh/id_rsa <<<y 2>&1 >/dev/null
cat ~/.ssh/id_rsa.pub

exit 0
